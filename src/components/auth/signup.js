import React, { Component } from 'react';
import {Field, reduxForm} from 'redux-form';
import { connect } from 'react-redux';
import { signupUser } from '../../actions';

class Signup extends Component {

    componentDidMount() {
        window.scrollTo(0,0);
    }

    handleFormSubmit(user) {
        this.props.signupUser(user, ()=> {
            this.props.history.push('/');
        });
    }


    renderField(field) {
        const { meta: { touched, invalid, error } } = field;
        const className = `from-group ${touched && error && invalid ? 'has-danger' : '' }`
        return (
            <div className={className}  style={{ textAlign: 'left' }}>
                <div className="col-sm-3" style={{marginTop: '19px'}}>
                    <label>{field.label}</label>
                </div>
                 <div className="col-sm-9" style={{marginTop: '19px'}}>
                    <input 
                        className="form-control"
                        type={field.type}
                        { ...field.input }
                    />
                </div>
                <div className="text-help" >
                    { touched ? error : '' }
                </div>
            </div>
        );
    }

    render() {
        const { handleSubmit, errorMessage } = this.props;

        const errorInLogin = errorMessage  ?   <div className="alert alert-danger" >
                                                    <strong>Oops!</strong> {errorMessage}
                                                </div>
                                            : '';

        return (
            <div className="card col-sm-offset-2 col-sm-8" style={{ display: 'inline-flex', marginBottom: '3rem' }} > {/*margin: '4rem 1rem'*/}
                <div className="card-block" style={{textAlign: 'center'}} >
                    <form onSubmit={handleSubmit( this.handleFormSubmit.bind(this) )} >
                        <h3>Signup</h3>

                        <Field
                            label="Nombre"
                            name="nombre"
                            type="text"
                            component={this.renderField}
                        />
                        <Field
                            label="Teléfono"
                            name="telefono"
                            type="text"
                            component={this.renderField}
                        />
                        <Field
                            label="Email"
                            name="email"
                            type="text"
                            component={this.renderField}
                        />

                        <Field
                            label="Password"
                            name="password"
                            type="password"
                            component={this.renderField}
                        />

                        <Field
                            label="Confirm Password"
                            name="passwordConfirm"
                            type="password"
                            component={this.renderField}
                        />

                        <div className="col-xs-12" style={{marginTop: '30px'}}>                            
                            
                            { errorInLogin }

                            <button type="submit" className="btn btn-primary" >
                                Signup 
                            </button>
                        </div>
                    </form>
                </div>        
            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    if(!values.nombre) {
        errors.nombre = "Ingresa tu nombre";        
    }
    if(!values.telefono) {
        errors.telefono = "Ingresa tu teléfono";        
    }

    if(!values.email) {
        errors.email= "Ingresa tu email";
    } else {
        // si existe email revisar q sea valido
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        console.log(re.test(values.email));
        if(!re.test(values.email)) {
            errors.email= "Ingresa un email valido";
        }
    }

    if(values.password !== values.passwordConfirm) {
        errors.password = "El password necesita coincidir";        
    }

    if(!values.password) {
        errors.password= "Ingresa tu password";
    }

    if(!values.passwordConfirm) {
        errors.passwordConfirm = "Please write the confimation password";
    }



    return errors;
}

function mapStateToProps(state) {
    return { errorMessage: state.auth.error };
}


export default reduxForm ({ 
    form: 'SignupForm',
    validate 
})
( connect(mapStateToProps, { signupUser }) (Signup) );