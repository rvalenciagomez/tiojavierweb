import React, { Component } from 'react';
import {Field, reduxForm} from 'redux-form';
import { connect } from 'react-redux';
import { signinUser } from '../../actions';

class Signin extends Component {

    componentDidMount() {
        window.scrollTo(0,0);
    }
    
    handleFormSubmit({email, password}) {
        // console.log(email , password);
        this.props.signinUser({email, password}, ()=> {
            this.props.history.push('/');
        });
    }


    renderField(field) {
        const { meta: { touched, invalid, error } } = field;
        const className = `from-group ${touched && error && invalid ? 'has-danger' : '' }`
        return (
            <div className={className}  style={{ textAlign: 'left' }}>
                <div className="col-sm-3" style={{marginTop: '19px'}}>
                    <label>{field.label}</label>
                </div>
                 <div className="col-sm-9" style={{marginTop: '19px'}}>
                    <input 
                        className="form-control"
                        type={field.type}
                        { ...field.input }
                    />
                </div>
                <div className="text-help" >
                    { touched ? error : '' }
                </div>
            </div>
        );
    }

    render() {
        const { handleSubmit, errorMessage } = this.props;

        // console.log('auth: ', this.props.authenticated);
        // const errorInLogin = () => {
        //     if(errorMessage) {
        //         console.log('error message', errorMessage);
        //         return (
        //             <div className="alert alert-danger" >
        //                 <strong>Oops!</strong> {errorMessage}
        //             </div>
        //         );
        //     }
        // }

        const errorInLogin = errorMessage  ?   <div className="alert alert-danger" >
                                                    <strong>Oops!</strong> {errorMessage}
                                                </div>
                                            : '';

        return (
            <div className="card col-sm-offset-2 col-sm-8" style={{ display: 'inline-flex', marginBottom: '3rem' }} > {/*margin: '4rem 1rem'*/}
                <div className="card-block" style={{textAlign: 'center'}} >
                    <form onSubmit={handleSubmit( this.handleFormSubmit.bind(this) )} >
                        <h3>Signin</h3>

                        <Field
                            label="Email"
                            name="email"
                            type="text"
                            component={this.renderField}
                        />

                        <Field
                            label="Password"
                            name="password"
                            type="password"
                            component={this.renderField}
                        />

                        <div className="col-xs-12" style={{marginTop: '30px'}}>                            
                            
                            { errorInLogin }

                            <button type="submit" className="btn btn-primary" >
                                Signin 
                            </button>
                        </div>
                    </form>
                </div>        
            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    if(!values.email) {
        errors.email= "Ingresa tu email";
    } else {
        // si existe email revisar q sea valido
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        console.log(re.test(values.email));
        if(!re.test(values.email)) {
            errors.email= "Ingresa un email valido";
        }
    }

    if(!values.password) {
        errors.password= "Ingresa tu password";
    }

    return errors;
}

function mapStateToProps(state) {
    return { errorMessage: state.auth.error };
}


export default reduxForm ({ 
    form: 'SigninForm',
    validate 
})
( connect(mapStateToProps, { signinUser }) (Signin) );