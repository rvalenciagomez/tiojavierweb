import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions';

class Logout extends Component {
    componentWillMount() {
        this.props.logoutUser();    
    }

    render() {
        return (
            <div className="col-xs-offset-2 col-xs-8 gracias-visita" >
                Gracias por tu visita!
            </div>
        );
    }
}


export default connect(null, { logoutUser }) (Logout);