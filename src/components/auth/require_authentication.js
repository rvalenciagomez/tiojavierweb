import React, { Component } from 'react';
import { connect } from 'react-redux';

export default function(ComposedComponent) {
    class Authentication extends Component {
        static contextTypes = {
            router: React.PropTypes.object
        }

        componentWillMount() {
            let role = null;
            const profile = JSON.parse(localStorage.getItem('profile'));
            profile ? role = profile.role : role = null;

            if(!this.props.authenticated || role !== 'ADMIN') {
                this.context.router.history.push("/");
                console.log('desviado por require auth...');
            }
        }

        componentWillUpdate(nextProps) {
            let role = null;
            const profile = JSON.parse(localStorage.getItem('profile'));
            profile ? role = profile.role : role = null;

            if(!nextProps.authenticated || role !== 'ADMIN') {
                this.context.router.history.push("/");
                console.log('desviado por require auth UPDATED...');                
            }
        }

        render() {
            // console.log('context', this.context);
            return <ComposedComponent {...this.props} />
        }
    }

    function mapStateToProps(state) {
        return { authenticated: state.auth.authenticated }
    }

    return connect(mapStateToProps) (Authentication);
}