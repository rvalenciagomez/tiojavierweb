import React, {Component} from 'react';
import '../styles/footer.css';
import { Link } from 'react-router-dom';

class Footer extends Component {

    render() {
        return (
            <div>
                {/*footer*/}
                <div className="row col-xs-12 footer-general">
                    <div className="footer__inner">
                        <div className="footer__cols">
                            <div className="footer__col">
                                <ul className="footer__list">
                                    <li className="footer__list-item">
                                        <strong>SIMMAT</strong>
                                    </li>
                                    <li>
                                        <Link to="/quienes-somos" style={{color: 'white'}} >Empresa</Link>
                                    </li>
                                    <li>
                                        <p></p>
                                    </li>
                                    <li>
                                        <p>
                                            Email: <a href="mailto:contacto@simmat.mx" style={{color: 'white'}} >
                                                contacto@simmat.mx</a>
                                         </p>
                                    </li>
                                    <li>
                                        <p> Direccion: </p>
                                        <p>Ciudad de México, Mexico.</p>
                                    </li>
                                </ul>
                            </div>  

                            <div className="footer__col">
                                <ul className="footer__list">
                                    <li className="footer__list-item">
                                        <strong>Producto</strong>
                                    </li>
                                    <li>
                                        <p>
                                            <Link to="/catalogo" style={{color: 'white'}} >Catálogo</Link>
                                        </p>
                                    </li>
                                    <li>
                                        <p></p>
                                    </li>
                                </ul>
                            </div>

                            <div className="footer__col">
                                <ul className="footer__list">
                                    <li className="footer__list-item">
                                        <strong>Leer más</strong>
                                    </li>
                                    <li>
                                        <p>Facebook (proximamente)</p>
                                    </li>
                                    <li>
                                        <p></p>
                                    </li>
                                    <li>
                                        <p></p>
                                    </li>
                                </ul>
                            </div>

                            <div className="footer__col">
                                <ul className="footer__list">
                                    <li className="footer__list-item">
                                        <strong>Asistencia</strong>
                                    </li>
                                    <li>
                                        <p>
                                            <Link to="/add-contacto" style={{color: 'white'}} >Contacto</Link>
                                        </p>
                                    </li>
                                    <li>
                                        <p>Disponible de 9am - 6pm,</p><p> de lunes a viernes.</p>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;