
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { crearContacto } from '../../actions';
// import { NotificationContainer, NotificationManager } from 'react-notifications';
import { Notification } from 'react-notification';
// import 'react-notifications/lib/notifications.css';
import '../../index.css';

class AddContacto extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isActive: false
        }
    }

    componentDidMount() {
        window.scrollTo(0,0);
    }

    toggleNotification() {
        this.setState({
            isActive: !this.state.isActive
        });
        console.log('is active', this.state.isActive);
    }

    onSubmit(values) {
        this.props.crearContacto(values, () => {
            this.toggleNotification();
        });
    }

    renderField(field) {
        const { meta: { touched, invalid, error } } = field;
        // console.log('value', val);
        const className = `col-xs-12 ${ touched && error && invalid ? 'has-danger': '' }`;
        // const valueInput = val ? val : '';

        let inputTypeContent = ( <input className="form-control"
                                        type={field.type} 
                                        {...field.input}
                                    /> );
        if(field.type == 'textarea') {
            inputTypeContent = ( 
                    <textarea className="form-control"
                              name={field.name}
                              {...field.input}
                              cols="30" rows="3" /> );
        } else  if(field.type == 'email') {
            inputTypeContent = ( <input className="form-control"
                                        type="text" 
                                        {...field.input}
                                        pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                                        required  
                                    /> );
        }
        

        return (
         <div className={className} style={{ textAlign: 'left' }} >
                <div className="col-sm-3" style={{marginTop: '19px'}}>
                    <label>{field.label} </label>
                </div>
                <div className="col-sm-9" style={{marginTop: '19px'}}>
                    { inputTypeContent }
                </div>
                <div className="text-help">
                    { touched ? error : ''  }
                </div>
        </div>
        );
    }

    render() {
        const { handleSubmit, initialValues } = this.props;
        const { isActive } = this.state;

        return (
            <div>
                 <div className="card col-sm-offset-2 col-sm-8" style={{ display: 'inline-flex', marginBottom: '3rem' }} > {/*margin: '4rem 1rem'*/}
                    <div className="card-block" style={{textAlign: 'center'}} >
                        <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) } >
                            <h4>Contactanos</h4>

                            <Field 
                                label="Nombre"
                                name="nombre"
                                type="text"
                                component={this.renderField}
                            />
                            <Field 
                                label="Teléfono"
                                name="telefono"
                                type="text" 
                                component={this.renderField}
                            />
                            <Field 
                                label="Email"
                                name="email"
                                type="email"                                
                                component={this.renderField}
                            />
                            <Field 
                                label="Comentario"
                                name="comentario"
                                type="textarea"
                                component={this.renderField}
                            />
                            <Field 
                                label="Empresa"
                                name="empresa"
                                type="text"
                                component={this.renderField}
                            />

                            <div className="col-xs-12" style={{marginTop: '30px'}}>                            
                                <button className="btn btn-primary" 
                                    type="submit" >
                                    Contactar
                                </button>
                            </div>

                        </form>
                        <Notification
                            isActive={this.state.isActive}
                            message="Mensaje Enviado exitosamente"
                            action="Dismiss"
                            title="Mensaje Enviado!"
                            dismissAfter={5000}
                            onDismiss={this.toggleNotification.bind(this)}
                            onClick={() =>  this.setState({ isActive: false })}
                        />

                        {/*<button type="button"
                            onClick={this.toggleNotification.bind(this)}
                            children={!isActive ? "Show notification" : "Hide notification"} 
                            />*/}
                    </div>
                </div>
            </div>
        );
    }

}

function mapStateToProps({contactos, auth}) {
    let contacto = null;
    if(auth.profile) {
        contacto = {
            nombre: auth.profile.nombre,
            telefono: auth.profile.telefono,
            email: auth.profile.email
        };
    }

    return { 
        initialValues: contacto
    }
}

function validate(values) {
    const errors = {};

    if(!values.nombre) {
        errors.nombre= "Ingresa tu nombre";
    }
    if(!values.telefono) {
        errors.telefono= "Ingresa tu telefono";
    }
    if(!values.email) {
        errors.email= "Ingresa tu email";
    } else {
        // si existe email revisar q sea valido
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        console.log(re.test(values.email));
        if(!re.test(values.email)) {
            errors.email= "Ingresa un email valido";
        }
    }
    return errors;
}


AddContacto = reduxForm({
        validate: validate,
        form: 'AddContactoForm'
        })(AddContacto);

    AddContacto = connect( mapStateToProps,{crearContacto })(AddContacto) ;

    export default AddContacto;