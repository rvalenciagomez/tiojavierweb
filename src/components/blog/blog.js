
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchArticulos, fetchCategorias } from '../../actions';
import { Link, Redirect } from 'react-router-dom';
import ArticulosList from './articulos-list';
import Select from 'react-select';
import _ from 'lodash';
import '../../styles/blog.css'

class Blog extends Component {
    constructor(props) {
        super(props);

        this.state= {
            redirectToRef: false,
            categoria: null // se inicia en null
        }
    }

    componentDidMount() {
        window.scrollTo(0,0);
    }

    componentWillMount() {
        this.props.fetchCategorias();
    }

    getCategoriaSelected(categSelected) {
        console.log('categSelected selected', categSelected);
        this.setState({
            categoria: categSelected
        });
    }


    render() {
        const { authenticated, categorias } = this.props;
        let role = null;
        const profile = JSON.parse(localStorage.getItem('profile'));
        profile ? role = profile.role : role = null;
        
        // console.log('categorias',categorias);
        const renderCategorias = categorias 
                ? (
                        <div className="form-group col-xs-3"  style={{marginTop: '1rem'}}>
                            <Select className="selectStyle"
                                name="categoria"
                                value={this.state.categoria}
                                options={categorias}
                                onChange={this.getCategoriaSelected.bind(this)} 
                                placeholder="Categorias de Articulos.."
                            /> 
                        </div>
                ) : '';
        
        const nuevoArticuloIfAuth = authenticated && role === 'ADMIN'
                ? (
                    <div className="col-xs-3" style={{textAlign: 'right', marginTop: '1rem'}}>
                        <Link className="btn btn-primary" to="/catalogo/add-articulo"  >
                            Nuevo Articulo
                        </Link>
                    </div>
                ) : '';

        return (
            <div>
                <div className="col-xs-12" style={{textAlign: 'left'}}>                
                    { renderCategorias }
                    
                    <div className="col-xs-6" style={{ textAlign:'center'}}>
                        <h2 >Catálogo</h2>
                    </div>
                    
                    { nuevoArticuloIfAuth }
                </div>            
                {/*renderizar lista de articulos*/}
                
                <div className="col-xs-12" style={{padding: '0 2em', margin: '2rem 0', background:'#FF4F00'}}>                    
                    {/*{this.renderArticulos()}*/}
                    <ArticulosList {...this.props }
                        categoria={this.state.categoria}    
                     />
                </div>
                
            </div>

        );
    }
}

function mapStateToProps({ articulos, auth, categorias }) {
    let categoriasArr = [];
    if(categorias) {
        categoriasArr = Object.keys(categorias).map(key => {
                return { label: categorias[key].nombre, value: key }
            });
    }

    return { 
        articulos: articulos,
        authenticated: auth.authenticated,
        categorias: categoriasArr
    }
}

export default connect( mapStateToProps, { fetchArticulos, fetchCategorias } )(Blog);
