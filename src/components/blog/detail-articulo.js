import React, {Component} from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import { detailArticulo, deleteArticulo } from '../../actions';
import { AMAZON_ROUTE } from '../../actions/constantes';
import _ from 'lodash';
import '../../styles/detail-articulo.css'

class DetailArticulo extends Component {
    componentDidMount() {
        if(!this.props.articulo) {
            const { id } = this.props.match.params;
            this.props.detailArticulo(id);
        }
    }

    onEliminarArticulo(key) {
        this.props.deleteArticulo(key, () => {
            this.props.history.push('/catalogo')
        });
    }

    imagesToRender({ filesName }) { 
            console.log('imagenes ',filesName);

            return _.map(filesName, (file)=> {
                return (
                        <img  src={`${AMAZON_ROUTE}${file ? file: ''}`} 
                            className="image-detail"
                            key={filesName.indexOf(file)} />
                );
            });
        }

    render() {
        const { authenticated } = this.props;
        const { articulo } = this.props;
        
        let role = null;
        const profile = JSON.parse(localStorage.getItem('profile'));
        profile ? role = profile.role : role = null;

        var htmlContent = '<p></p>';
        if(!articulo) {
            return <div>...loading</div>
        } 

        console.log('articulo en detail', articulo);

        var htmlContent =  articulo.descripcion_detallada;

        const editarYEliminarIfAuth = () => {
            if(authenticated && role === 'ADMIN') {
                return (
                    <div className="col-xs-12" style={{marginBottom: '2em', textAlign: 'right'}}>
                        <Link className="btn btn-primary" 
                                to={"/catalogo/edit-articulo/"+articulo.key} >Editar</Link> 
                        {/*eliminar*/}
                        <button className="btn btn-danger"  style={{marginLeft: '1rem'}}
                                onClick={() => this.onEliminarArticulo(articulo.key)} >
                                Eliminar
                        </button>             
                    </div>
                );
            }
        }

       return (
        <div>
            <div className="col-xs-12">
                <Link to="/catalogo" className="text-xs-right" style={{ float:'right', marginRight: '3rem'}}  >
                    Regresar a blog</Link>        

                <h2 className="col-xs-12" style={{fontWeight: '600', textAlign: 'center'}}>
                    {articulo.titulo}
                </h2>
                <div className="row col-sm-12" style={{padding: 0, backgroundColor:'#FF4F00', color:'white', margin:'0px', textAlign: 'center' }}>
                    <div className="col-sm-12" >
                        <h3 className="titulo-box-detail" >{articulo.descripcion}</h3>
                        {/*imagenes*/}
                        { this.imagesToRender(articulo) }
                    </div>
                </div>
            </div>
            <div className="col-xs-12 detail-container ">
                <div className="col-xs-12 col-md-offset-2 col-md-8 detail-body" style={{}}>
                    <div
                        ref= { ref => ref ? ref.innerHTML = htmlContent : null } >
                    </div>
                </div>

                { editarYEliminarIfAuth() }
            </div>
        </div>
        );
    }

}

function mapStateToProps({articulos, auth: {authenticated}}, ownProps) {
    return { 
        articulo: articulos[ ownProps.match.params.id ],
        authenticated: authenticated
    };
}

export default connect(mapStateToProps, { detailArticulo, deleteArticulo })(DetailArticulo);