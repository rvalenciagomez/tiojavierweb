import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchArticulos } from '../../actions';
import { Link, Redirect } from 'react-router-dom';
import { AMAZON_ROUTE } from '../../actions/constantes';
import _ from 'lodash';
import '../../styles/blog.css'

class ArticulosList extends Component {
    constructor(props) {
        super(props);

        // console.log('props',props);

        this.state= {
            redirectToRef: false,
            tienePrioridad: props.tienePrioridad ? props.tienePrioridad : false
        }
    }

    componentDidMount() {
        console.log('disparando fetch articulos');
        this.props.fetchArticulos();
    }

    redirectToDetail(id) {
        this.props.history.push('/catalogo/'+id);
    }

    renderArticulos(){
        const { categoria } = this.props;
        if(this.state.redirectToRef) {
            return(
                <Redirect to={'/catalogo/add-articulo'} />
            );
        }
        
        return _.map(this.props.articulos, articulo => {
            let showImage = <div></div>;
            if(articulo.filesName && articulo.filesName.length != 0 ) {
                if(articulo.filesName.length > 1) {
                    showImage = (<img  src={`${AMAZON_ROUTE}${articulo.filesName[1]}`}
                                className="image-card" />);
                } else  {
                    showImage = (<img  src={`${AMAZON_ROUTE}${articulo.filesName[0]}`}
                                className="image-card" />);
                }
            }

            // si requiere prioridad y el articulo no tiene prioridad
            if( !articulo.destacado && this.state.tienePrioridad ) {
                return '';
            }
            // si hay filtro de categoria  
            // Ó  la categoria filtro coincide con la cat del articulo
            if(!categoria || (categoria && articulo.categoria && categoria.value === articulo.categoria.value)) {
                return (
                    <div className="card card-list" style={{cursor:'pointer'}}
                        key={articulo.key}
                        onClick={()=> {this.redirectToDetail(articulo.key)}} >
                        <div className="card-block">
                            {/*imagen*/}
                            {showImage}
                            {/*titulo*/}
                            <h4>{articulo.titulo}</h4>
                            {/*descripcion corta*/}
                            <div className="description">
                                {articulo.descripcion}
                            </div>
                        </div>
                    </div>
                );
            }
        });
    }

    render() {
        // console.log('posts in index', this.props);
        return (
            <div className="row col-xs-12" style={{padding: '0', margin: '2rem 0'}}>
                {this.renderArticulos()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { 
        articulos: state.articulos
    }
}

export default connect( mapStateToProps, { fetchArticulos } )(ArticulosList);