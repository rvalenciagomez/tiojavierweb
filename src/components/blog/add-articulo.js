import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { createArticulo, fetchCategorias } from '../../actions';
import TinyMCE from 'react-tinymce';
// import { CategoriasArticulo as categorias } from './categorias-articulo';
// importaciones para select
import Select from 'react-select';

import 'react-select/dist/react-select.css';
const EventEmitter = require('events');


// let initValuesEmitter = new EventEmitter();

class AddArticulo extends Component {
    constructor(props) {
        super(props);

        //preparar opciones que es lista de categorias para select options
        // let options = categorias.map(categoria => {
        //     return { label: categoria.nombre, value: categoria.id }
        // })

        this.state = {
            categoria: props.categorias[0],
            initialContent: ''
        };

        console.log('categoria state', this.state.categoria);

        // initValuesEmitter.once('newEvent', (initialValues, listener) => {
        //     console.log('initial values antes', initialValues);
        //     if(initialValues.categoria) {
        //         this.setState({
        //             categoria_id: initialValues.categoria.id,
        //             initialContent: initialValues.descripcion_detallada
        //         });
        //     }
        // });
    }
    
    componentWillMount() {
        // traer categorias
        this.props.fetchCategorias();
    }

   
    onSubmit(values) {
        const { categorias } = this.props;

        //editar descripcion detallada
        values.descripcion_detallada = this.state.initialContent;        
        console.log('values on submit', values);

        //editar categoria selected
        // const categ = categorias.find( categoria => categoria.value === articulo.categoria.value  );
        // console.log('categ', categ);
        if(this.state.categoria) {
            values.categoria = this.state.categoria;
        }
        console.log('articulo to submit', values);

        // event.preventDefault();
        this.props.createArticulo(values, ()=> {
            this.props.history.push('/catalogo');
        });
    }

     renderField(field) {
        const { meta: { touched, error }} = field;
        const className = `row form-group ${touched && error ? 'has-danger' : ''}`
        return (
            <div className={className}>   
                <div className="col-xs-12">
                    <label >{field.label}</label>
                </div>
                <div className="col-xs-12">
                    <input type="text" className="form-control"
                        {...field.input} />
                </div>
                <div className="text-help" > 
                    { touched ? error : '' }
                </div>
            </div>
        );
    }

   
    handleEditorChange = (e) => {
        this.setState ({
            initialContent: e.target.getContent()
        });
           
        window.setTimeout(()=>{ 
            console.log('Content was updated:', this.state.initialContent);  
        }, 1000)
    }

    renderEditor(field) {
       
        const editorConfig =  {
            plugins: 'link,image,lists,paste,code',
            toolbar: 'undo redo | formatselect bullist numlist | alignleft aligncenter alignright alignjustify | bold italic link | image code paste',
            block_formats: 'Paragraph=p;Heading=h3',
            menubar: true,
            statusbar: false,
            body_class: 'editable-field-content',
            paste_word_valid_elements: 'b,strong,i,em,h1,h2,h3,p,li,ul,ol,a',
            paste_retain_style_properties: 'none',
            paste_strip_class_attributes: 'none',
            paste_remove_styles: true,
        }

        return(
            <TinyMCE
                    {...field.input}
                    content={this.state.initialContent}
                    config={editorConfig}
                    onBlur={e=> this.handleEditorChange(e)}
                />
        );
           /* <TextEditor 
                content={this.state.initialContent}
                
            />*/
    }

    getCategoriaSelected(categSelected) {
        const { categorias } = this.props;
        //editar categoria selected
        const categ = categorias.find( categoria => categoria.value === categSelected.value  );
        console.log('categ', categ);
        
        this.setState({
            categoria: categ
        });
        // setTimeout(() => {
        //     console.log('categoria en articulo', this.state.categoria);
        // }, 1000);
    }


    render() {
        const { handleSubmit, categorias }  = this.props;
        
        let categoria_id;
        if(categorias.length === 0 && this.state.categoria) {
            return <div>Loading...</div>;
        } else {
            categoria_id = this.state.categoria;
        }

        return (
            <div>
                <div className="card col-sm-offset-2 col-sm-8" style={{margin: '4rem 1rem'}} >
                    <div className="card-block" >
                        <h4>Crear Articulo</h4>
                            <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) } >
                                <Field 
                                    label="Titulo de Articulo"
                                    name="titulo"
                                    component={this.renderField} 
                                     />

                                <Field 
                                    label="Descripción Corta"
                                    name="descripcion"
                                    component={this.renderField} />

                                <div className="row form-group">   
                                    <div className="col-xs-12">
                                        <label >Descripción Detallada</label>
                                    </div>
                                    <div className="col-xs-12">
                                       <Field 
                                            label="Descripción Detallada"
                                            name="descripcion_detallada"
                                            id="descripcion_detallada"
                                            component={ this.renderEditor.bind(this) } 
                                        />
                                    </div>
                                </div>


                                <div className="row form-group">   
                                    <div className="col-xs-12">
                                        <label >Categoria de Articulo</label>
                                    </div>
                                    <div className="col-xs-12">
                                        <Select
                                            name="categoria"
                                            id="categoria"
                                            value={categoria_id}
                                            options={categorias}
                                            onChange={this.getCategoriaSelected.bind(this)} 
                                        /> 
                                    </div>
                                </div>
                                
                                <button className="btn btn-primary"
                                    type="submit" >
                                    Salvar Articulo
                                </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

} 

function validate(values) {
    const errors = {};

    if(!values.titulo) {
        errors.titulo = "Enter the correct titulo"
    }

    if(!values.descripcion) {
        errors.descripcion = "Enter the correct descripcion"
    }

    return errors;
}

function mapStateToProps({articulos, categorias}, ownProps) {
    let categoriasArr = [];
    if(categorias) {
        categoriasArr = Object.keys(categorias).map(key => {
            return { label: categorias[key].nombre, value: key }
        });
    }

    return { 
        // articulo: articulos[ ownProps.match.params.id ],
        categorias: categoriasArr
    }
}

export default reduxForm({
        validate: validate,
        form: 'AddArticuloForm'
    })(
    connect(mapStateToProps,{createArticulo, fetchCategorias })(AddArticulo)
    ) ;
