import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { detailArticulo, editArticulo, uploadFiles, editContent, deleteImage, fetchCategorias } from '../../actions';
import { AMAZON_ROUTE } from '../../actions/constantes';
import TinyMCE from 'react-tinymce';
// import { CategoriasArticulo as categorias } from './categorias-articulo';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import '../../styles/edit-articulo.css';
import _ from 'lodash';

const EventEmitter = require('events');


// let initialContent = '';
let initValuesEmitter = new EventEmitter();

class EditArticulo extends Component {
    
    constructor(props) {
        super(props);
        
        //preparar opciones que es lista de categorias para select options
        // let options = categorias.map(categoria => {
        //     return { label: categoria.nombre, value: categoria.id }
        // })

        this.state = {
            // categoria_id: this.props.categorias[0].key,
            // options: options,  //select options
            initialContent: ''
        };

        // console.log('categoria state', this.state.categoria_id);

        initValuesEmitter.once('newEvent', (initialValues, listener) => {
            console.log('initial values antes', initialValues);
            if(initialValues.categoria) {
                this.setState({
                    // categoria_id: initialValues.categoria.id,
                    initialContent: initialValues.descripcion_detallada
                });
            }
        });

    }

    componentWillMount() {
        // traer categorias
        this.props.fetchCategorias();

        const { id } = this.props.match.params;
        // trae articulo de id
        if(id) {
            this.props.detailArticulo(id);
        }
    }

   
    onSubmit(values) {
        const { id } = this.props.match.params;
        const { initialValues, categorias } = this.props;

        values.descripcion_detallada = this.state.initialContent;        
        
        if(initialValues.filesName) {
            values.filesName = initialValues.filesName;
        }

        //editar categoria selected
        const categ = categorias.find( categoria => categoria.value === initialValues.categoria.value  );
        
        values.categoria = categ;
        console.log('articulo to submit', values);

        this.props.editArticulo(values, ()=> {
            this.props.history.push('/catalogo/'+id );
        });
    }

    deleteImage(file, articulo ) {
        this.props.deleteImage(file, articulo );
    }
    
    renderImageField(articulo) {
        const imagesToRender = () => { 

            return _.map(articulo.filesName, (file)=> {
                return (
                    <div className="col-xs-3" key={articulo.filesName.indexOf(file)} >
                        <img  src={`${AMAZON_ROUTE}${file ? file: ''}`} 
                            className="image_edit" />
                        <div className="cambio_imagen" >
                            Cambiar Imagen:
                            <input type="file"  placeholder="Subir imagen..."
                                id="input-file" 
                                onChange={
                                    ( e ) => {      
                                    e.preventDefault();
                                    //find current index
                                    let indexFile = _.findIndex(articulo.filesName, fl => fl == file );
                                    // convert files to an array
                                    const files = [ ...e.target.files ];
                                    console.log('files', files);
                                    this.props.uploadFiles(indexFile, files,articulo);
                                    }
                                } /> 
                        </div>
                        {/*  - Agregar imagen -  */}
                        <div className="" >
                            <button type="button" 
                                onClick={ () => this.deleteImage(file, articulo ) }
                                className="btn btn-sm btn-danger"
                                style={{cursor:'pointer'}} >
                                Eliminar
                            </button>
                        </div>
                    </div>
                );
            });
        }
    
        return(
            <div className="row col-xs-12"  style={{marginTop: '1rem'}}  >	
				<div className="col-xs-12">
					<label>Imagen:</label>
				</div>
				<div className="col-xs-12" >
                    { imagesToRender() }
				</div>
                {/*  - Agregar imagen -  */}
                <div className="col-xs-12 agregar_imagen" >
                    Agregar Imagen:
                    <input type="file"  placeholder="Subir imagen..."
                        id="input-file" 
                        onChange={
                            ( e ) => {      
                            e.preventDefault();
                            // convert files to an array
                            const files = [ ...e.target.files ];
                            console.log('files', files);
                            this.props.uploadFiles(null, files,articulo);
                            }
                        } /> 
                </div>
			</div>
        );
    }

    /* render editor*/

    handleEditorChange = (e) => {
        this.setState ({
            initialContent: e.target.getContent()
        });
           
        window.setTimeout(()=>{ 
            console.log('Content was updated:', this.state.initialContent);  
         }, 1000)
    }

    renderEditor(field) {
       
        // inicializar valor de contenido
        // this.state.initialContent = this.getInitialState();
       
        const editorConfig =  {
            plugins: 'link,image,lists,paste,code',
            toolbar: 'undo redo | formatselect bullist numlist | alignleft aligncenter alignright alignjustify | bold italic link | image code paste',
            block_formats: 'Paragraph=p;Heading 3=h3',
            menubar: true,
            statusbar: false,
            body_class: 'editable-field-content',
            paste_word_valid_elements: 'b,strong,i,em,h1,h2,h3,p,li,ul,ol,a',
            paste_retain_style_properties: 'none',
            paste_strip_class_attributes: 'none',
            paste_remove_styles: true,
        }

        return(
            <TinyMCE
                {...field.input}
                content={this.state.initialContent}
                config={editorConfig}
                onBlur={e=> this.handleEditorChange(e)}
            />
        );
    }


    getCategoriaSelected(categSelected) {
        console.log('categSelected selected', categSelected);
        const { initialValues, categorias } = this.props;
        
        //editar categoria selected
        const categ = categorias.find( categoria => categoria.value === categSelected.value  );
        console.log('categ', categ);
        
        initialValues.categoria = categ;
        console.log('articulo to edit', initialValues);

        this.props.editArticulo(initialValues, ()=> {});
    }

    renderField(field) {
        const { meta: { touched, error }, type} = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        let inputTypeContent = 
            type === 'text' 
            ? ( <div>
                    <div className="col-xs-12">
                        <label >{field.label}</label>
                    </div>
                    <div className="col-xs-12">
                        <input type="text" className="form-control"
                            {...field.input} />
                    </div>
            </div>)
            : type === 'checkbox' 
                ? ( <div>
                        <div className="col-xs-12">
                            <label className="col-xs-6" style={{ margin:'1rem 0',paddingLeft: 0 }} >
                                {field.label}
                                <input type={type} className="form-control checkbox-style"
                                    {...field.input} />
                            </label>
                        </div>
                </div>)
                : '';

        return (
            <div className={className}>   
                { inputTypeContent }
                <div className="text-help" > 
                    { touched ? error : '' }
                </div>
            </div>
        );
    }

    render() {
        const { handleSubmit, initialValues, categorias }  = this.props;
        
        let categoria_id;
        if(!categorias || categorias.length === 0) {
            return <div>Loading...</div>;
        } else if(initialValues && initialValues.categoria) {
            categoria_id = initialValues.categoria.value;
        } else {
           return <div>Loading...</div>;
        }

        return (
            <div>
                <div className="card col-sm-offset-2 col-sm-8" style={{margin: '4rem 1rem'}} >
                    <div className="card-block" >
                        <h4>Editar Articulo</h4>
                            <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) } >
                                <Field 
                                    label="Titulo de Articulo"
                                    name="titulo"
                                    type="text"
                                    component={this.renderField} 
                                     />

                                <Field 
                                    label="Descripción Corta"
                                    name="descripcion"
                                    type="text"
                                    component={this.renderField} 
                                />
                               
                                {/* upload field*/}

                                {this.renderImageField(initialValues)}

                                {/*categorias*/}
                                <div className="col-xs-12" style={{padding: 0}}>
                                    <div className="form-group col-xs-6"  style={{marginTop: '1rem'}}>
                                        <label htmlFor="categoria">Categoria de Articulo</label>
                                        <Select
                                            name="categoria"
                                            value={categoria_id}
                                            options={categorias}
                                            onChange={this.getCategoriaSelected.bind(this)} 
                                        /> 
                                    </div>
                                    <div className="col-xs-6" style={{marginTop:'43px'}}>
                                        <Link to="/catalogo/edit-categorias" className="btn btn-secondary">
                                            Editar Categorias
                                        </Link>
                                    </div>
                                </div>

                                <Field 
                                    label="Publicacion Destacada"
                                    name="destacado"
                                    type="checkbox"
                                    component={this.renderField} 
                                />

                                <div className="form-group col-xs-12" style={{marginTop:'1rem'}}>
                                    <label >Descripción Detallada</label>
                                
                                    <Field 
                                        label="Descripción Detallada"
                                        name="descripcion_detallada"
                                        component={ this.renderEditor.bind(this) } 
                                    />
                                </div>
                                    
                                {/*button submin*/}
                                <button className="btn btn-primary"
                                    type="submit" >
                                    Salvar Articulo
                                </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

} 


function validate(values) {
    const errors = {};

    if(!values.titulo) {
        errors.titulo = "Enter the correct titulo"
    }

    if(!values.descripcion) {
        errors.descripcion = "Enter the correct descripcion"
    }

    /*if(!values.descripcion_detallada) {
        errors.descripcion_detallada = "Enter the correct descripcion_detallada"
    }*/

    return errors;
}

function mapStateToProps({articulos, categorias}, ownProps) {
    // console.log('new props',articulos);
    let categoriasArr = [];
    if(categorias) {
        categoriasArr = Object.keys(categorias).map(key => {
                return { label: categorias[key].nombre, value: key }
            });
    }

    return { 
        initialValues: articulos[ ownProps.match.params.id ], 
        categorias: categoriasArr
    }
}

// connect(mapStateToProps, mapDispatchToProps)(reduxForm(formOptions)(form))

    EditArticulo = reduxForm({
        validate: validate,
        form: 'EditArticuloForm'
        })(EditArticulo);

    EditArticulo = connect(
        mapStateToProps,
        {detailArticulo, editArticulo, uploadFiles, editContent, deleteImage, fetchCategorias }
    )(EditArticulo) ;

    export default EditArticulo;