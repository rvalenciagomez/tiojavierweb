import React, { Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { Editor, Raw } from 'slate';
import TinyMCE from 'react-tinymce';


export default class TextEditor extends Component {
  constructor(props) {
    super(props);
  }

  renderEditor(field) {
       
        const editorConfig =  {
            plugins: 'link,image,lists,paste,code',
            toolbar: 'undo redo | formatselect bullist numlist | alignleft aligncenter alignright alignjustify | bold italic link | image code paste',
            block_formats: 'Paragraph=p;Heading=h3',
            menubar: true,
            statusbar: false,
            body_class: 'editable-field-content',
            paste_word_valid_elements: 'b,strong,i,em,h1,h2,h3,p,li,ul,ol,a',
            paste_retain_style_properties: 'none',
            paste_strip_class_attributes: 'none',
            paste_remove_styles: true,
        }

        return(
            <TinyMCE
                {...field.input}
                content={this.props.content}
                config={editorConfig}
            />
        );
                {/*onBlur={e=> this.handleEditorChange(e)}*/}
    }

  render() {
    return (
      <Field 
          label="Descripción Detallada"
          name="descripcion_detallada"
          id="descripcion_detallada"
          component={ this.renderEditor.bind(this) } 
      />
    );
  }
} 