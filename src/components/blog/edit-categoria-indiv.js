import React, {Component}  from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { deleteCategoria, editCategoria } from '../../actions';


class EditCategoria extends Component {

    constructor(props) {
        super(props);
        // console.log('props de art indiv',props);

        this.state = { 
            categName: props.categoria.nombre,
            isEditable: false
        };
    }

    onDeleteCategoria(categoriaId) {
        this.props.deleteCategoria(categoriaId);
    }

    handleChange({ target: { value } }) {
        this.setState({
            categName: value
        });
    }

    onUpdateCategoria(categoria) {
        categoria.nombre = this.state.categName;
        // console.log('categ a actualizar',categoria);
        
        // salvar categoria actualizada
        this.props.editCategoria(categoria);
    }

    onCategoriaEditable(categoria) {
        this.setState({ isEditable: !this.state.isEditable });

        // encontrar y esconder div
        let ctg, ctgEd, btnCtg, btnCtgEd;
            ctg = document.getElementById(categoria.key);
            ctgEd = document.getElementById('edit-'+categoria.key);
            btnCtg = document.getElementById('btn-'+categoria.key);
            btnCtgEd = document.getElementById('btn-edit-'+categoria.key);
            
        if(this.state.isEditable) {
            ctg.style.display='none';
            ctgEd.style.display=null;
            btnCtg.style.display='none';
            btnCtgEd.style.display=null;
        } else {
            ctg.style.display=null;
            ctgEd.style.display='none';
            btnCtg.style.display=null;
            btnCtgEd.style.display='none';
        }
    }

    render() {
        const { categoria } = this.props;
        
        return(
            <div className="col-xs-offset-2 col-xs-8 item-categoria"  >
                
                <div id={categoria.key} 
                    className="col-xs-8" >
                    {categoria.nombre}
                </div>

                <div id={'edit-'+categoria.key} 
                    className="col-xs-8" style={{ display:'none'}} >

                    <input 
                        className="form-control"
                        type="text"
                        name={'nombre'+categoria.key}
                        value={this.state.categName }
                        onChange={ this.handleChange.bind(this) }
                    />    
                </div>

                <div className="col-xs-4">                     
                    <button style={{ marginRight:'10px'}} className="btn btn-primary" type="button" 
                        id={'btn-'+categoria.key}
                        onClick={ () => this.onCategoriaEditable(categoria) } >
                        Edit
                    </button>
                    <button style={{ marginRight:'10px', display:'none'}} 
                        className="btn btn-primary" type="button" 
                        id={'btn-edit-'+categoria.key}                            
                        onClick={ () => {
                            this.onUpdateCategoria(categoria),
                            this.onCategoriaEditable(categoria)
                            }} >
                        Save
                    </button>
                    <button type="button" 
                        onClick={ () => this.onDeleteCategoria(categoria.key) }
                        className="btn btn-danger" >
                        Eliminar
                    </button>
                </div>
            </div>
        );
    }
}

// function mapStateToProps(state) {
//     return { 
//         categoria: state.categoria
//     }
// }

export default connect(null, { deleteCategoria, editCategoria }) (EditCategoria);