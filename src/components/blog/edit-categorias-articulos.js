import _ from 'lodash';
import { connect } from 'react-redux';
import React, { Component } from 'react';
// import { CategoriasArticulo as categorias } from './categorias-articulo';
import { Field, reduxForm } from 'redux-form';
import { crearCategoria, fetchCategorias } from '../../actions';
import EditCategoria from './edit-categoria-indiv';
import '../../styles/categoria-articulos.css';

class EditCategoriasArticulos extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categName: ''
        }
    }

    componentWillMount() {
        this.props.fetchCategorias();
    }

    createCategoria() {
        const categ = { nombre: this.state.categName };
        this.props.crearCategoria(categ);
        // borrar state actual
        this.setState({
            categName: ''
        });
    }

    renderCategorias() {
        const { categorias } = this.props;
        
        return _.map(categorias, categoria => {
            return(
                <EditCategoria { ...this.props }
                    categoria={categoria} 
                    key={categoria.key}
                />
            );
        });
    }

    handleChange({target: {value} }) {
        this.setState({
            categName: value
        });
    }

    render() {
        const { handleSubmit } = this.props;
        
        return (
            <div className="col-xs-12" style={{textAlign:'center', margin:'2rem 0'}} >
                <h3>Categorias de Articulos</h3>
                {/* <form onSubmit={ handleSubmit(this.createCategoria.bind(this)) } > */}
                    <div className="col-xs-offset-2 col-xs-8" style={{textAlign: 'right', marginBottom:'2rem', marginTop:'2rem'}}>
                        <div className="col-xs-9">
                            <input 
                                className="form-control"
                                type="text"
                                name="nombre"
                                placeholder="Nombre nueva categoria..."
                                value={this.state.categName}
                                onChange={ this.handleChange.bind(this) }
                            />
                        </div>
                        <div className="col-xs-3">                    
                            <button type="submit" className="btn btn-primary"
                                onClick={this.createCategoria.bind(this)} >
                                Nueva Categoria
                            </button>
                        </div>
                    </div>
                {/* </form> */}
                
                { this.renderCategorias() }

            </div>
        );
    }
}

function mapStateToProps({categorias}) {
    return { categorias };
}

export default reduxForm({
    form: 'EditCategoriaForm'
})(
    connect(mapStateToProps, { crearCategoria, fetchCategorias }) (EditCategoriasArticulos)
);