import React, {Component} from 'react';
import '../styles/menu.css'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { onUserAlreadyLogged } from '../actions/index'
import logo from '../assets/images/logos/SIMMAT2.png'



class Menu extends Component {
    constructor(props) {
        super(props);
        let lastScrollTop = 0;
        this.state = {
            isScrollDown: false
        }


        document.addEventListener('scroll', () => {
            var st = window.pageYOffset || document.documentElement.scrollTop;
            if(st > lastScrollTop) {
                this.setState({ isScrollDown: true });
            } else {
                this.setState({ isScrollDown: false });
            }
        });
    }

    componentWillMount() {
        // checar usuario authenticado
        this.props.onUserAlreadyLogged();
    }
    
    render() {
        const titulo = 'SIMMAT';
        const { authenticated, profile } = this.props;
        const profileName = profile ? profile.nombre : '(Profile Name)';
        
        // clases dinamicas
        const navClasses = `navbar navbar-toggleable-md navbar-light custom-nav 
                ${this.state.isScrollDown ? 'nav-menu navbar-shrink' : '' }`;
        const imgClasses = `img-nav ${this.state.isScrollDown ? 'img-shrink' : ''}`
        const divClasses = `col-xs-12 ${this.state.isScrollDown ? 'div-space' : ''}`        


        let authRender = () => {
            if(authenticated === false) {
                return [
                    <li className="nav-item" key={1} >
                        <Link className="nav-link" to="/signin" >Login</Link>
                    </li>,
                    <li className="nav-item" key={2} >
                        <Link className="nav-link" to="/signup" >Registro</Link>
                    </li>
                ];
            } else {
                return(
                    <li className="nav-item dropdown" >
                        <Link className="nav-link dropdown-toggle" data-toggle="dropdown" style={{color:'rgba(0,0,0,.5)'}}
                                to="/" >{profileName}</Link>
                        <ul className="dropdown-menu">
                            <li>
                                <Link className="nav-link" to="/logout" >Logout</Link>
                            </li>
                        </ul>
                        
                    </li>
                );
            }
        }

        return(
           <div>
             <nav className={navClasses}> 
               {/* <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>*/}
                {/* Brand */}
                <div className="brand-name-wrapper" style={{width:'310px'}}>
                    {/* [routerLink] = "['/']" */}
                    <Link className="navbar-brand" className="custom-nav-brand"
                            to="/">
                        {/*{titulo}*/}
                        <img src={logo} className={imgClasses}/>
                    </Link>
                </div>

                <div className="side-menu-container" id="navbarsExampleDefault" style={{width: '100%', marginBottom:'-30px'}}>
                    <ul className="nav navbar-nav navbar-right nav-personal-align-rigth ">
                        <li className="nav-item">
                            <Link className="nav-link" to="/" >Home <span className="sr-only">(current)</span></Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/catalogo" >Catalogo</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/quienes-somos" >Quienes Somos</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/add-contacto" >Contacto</Link>
                        </li>

                        { authRender() }
                    
                    </ul>
                
                </div>
            </nav> 
            <div className={divClasses}>
            </div>        
           </div> 
        );
    }
}

function mapStateToProps(state) {
    return { 
        authenticated: state.auth.authenticated,
        profile: state.auth.profile
    };
}

export default connect(mapStateToProps, { onUserAlreadyLogged }) (Menu);