import React, {Component} from 'react'; 
import '../styles/home.css'

import banner from '../assets/images/banners/banner_rails.jpg';
import img_1_grupo_personas from '../assets/images/1_grupo_personas.jpg';
import img_2_materiales from '../assets/images/2_manejo_materiales.jpg';
import img_3_transportadora_bandas from '../assets/images/3_transportadora_bandas.jpg';

class QuienesSomos extends Component {

    componentDidMount() {
        window.scrollTo(0,0);
    }
    
    renderBoxes(image1, descripcion1, image2, descripcion2, backgndColor, tituloDeBox) {
        var descFunc1 = () => {
            return (
                <p> Contamos con personal altamente capacitado y con proveedores de clase mundial para poder ofrecer propuestas y soluciones de la mejor calidad. 
                    <br/>Nuestra empresa cuenta con gran experiencia en la industria y contamos con la gran ventaja de ser expertos en todos los componentes necesarios para el manejo de materiales. 
                    <br/>El personal de SIMMAT cuenta  con mas de 20 años de experiencia en este ramo.`
                </p>);
        }

        var descFunc2 = () => {
            return (
                <p> 
                    1) Distribuidor de componentes para Bandas transportadoras <br/>
                    2) Instalaciones y Servicios<br/>
                    3) Fabricante de componentes ´Mecánicos (Rodillos Nacionales) <br/>
                    4) Diseño y fabricación de equipos<br/>
                    5) Proyectos de manejo de materiales y  de energía sustentable<br/>
                    6) Distribuidor de Componentes y refacciones forjadas y en  fundición para equipo pesado y componentes para  transportadores de cadena, escaleras eléctricas, cadenas de transmisión y poleas para cadenas transmisión.<br/>
                </p>);
        }

        var descFunc3 = () => {
           return (
                <p>
                    <div style={{marginTop:'-50px'}}>
                        <br/>SIMMAT está localizada estratégicamente en la ciudad de México para cubrir el territorio Mexicano y Centro América.  También contamos con oficinas en Charlotte Carolina del Norte, Estados Unidos para proyectos Internacionales.  Fabricamos en México y Estados Unidos dependiendo  del proyecto.
                        <br/><br/>
                        <strong>SIMMAT se divide en 4 áreas principales:</strong>
                    
                        <br/> <br/>
                    
                        <div style={{ textAlign:'left'}}>
                            1- Desarrollo de proyectos de manejo de materiales a granel sólido (Diseño, Ingeniería, y Fabricación)<br/>
                            2- Representaciones de componentes y equipos para el manejo de solidos (Transportación, rodillos, bandas, cadenas, cangilones etc.)<br/>
                            3- Instalaciones y commissioning (Puesta en marcha ) de cualquier proyecto en manejo de gráneles sólidos.<br/>
                            4- Representaciones exclusivas en para México & Centro América. <br/>
                        </div>
                    </div>
                </p>
            );
        }

        const img1 = image1 
                    ? <img src={image1} className="card_img_custom" height="255" width="391"/>
                    : '';
        const img2 = image2
                    ? <img src={image2} className="card_img_custom" height="255" width="391"/>
                    : '';

        const desc1 = descripcion1 === 'desc1' 
                        ? descFunc1() 
                        : descripcion1 === 'desc3' ?  descFunc3() : '';
        const desc2 = descripcion2 === 'desc2' ? descFunc2() : '';

        // si backgrnd color naranja, entonces letras blancas
        const colorLetra = backgndColor !== 'white' ? 'white' : '';
        return (
            <div className="content-wapper--inner-thinner-sides">
                    <div className="row col-sm-12" style={{padding: 0, backgroundColor:backgndColor, color:colorLetra, margin:0}}>
                        <div className="col-sm-12" >
                            <h3 className="titulo-box centered" >{ tituloDeBox }</h3>
                        </div>

                        <div className="col-sm-6">
                    {/*Planifica tus actividades*/}
                            
                            <div className="card_item">
                            <div className="card__inner">
                                <div className="card_title">
                                <h3>{/*Planifica tus actividades*/}</h3>
                                </div>
                                <div className="card__image">
                                    {img1}
                                </div>

                                {desc1}
                            </div>
                            </div>

                        </div>

                        <div className="col-sm-6">
                        {/*Titulo*/}

                            <div className="card_item">
                                <div className="card__inner">
                                    <div className="card_title">
                                    {/*<h3>TITULO</h3>*/}
                                    </div>
                                    <div className="card__image">
                                        {img2}
                                    </div>

                                    {desc2}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        );
    }


    render() {    
        const tituloBox1 = 'Manejo de Materiales Eficiente!';
        const tituloBox2 = 'Materials Handling Systems and Components Experts, S.A.S';
        return (
            <div>
                
                 {/*Seccion 2*/}
                

                {/*------ grid of boxes -----------------*/}
                {/*image1, descripcion1, image2, descripcion2, backgndColor, tituloDeBox*/}
                { this.renderBoxes(null, 'desc3', img_3_transportadora_bandas, null, 'white', tituloBox2) }

                { this.renderBoxes(img_2_materiales, null, null, 'desc2', '#FF4F00') }
                
                {/*------ end // grid of boxes -----------------*/}
                
            </div>
        );
    }
}

export default QuienesSomos;