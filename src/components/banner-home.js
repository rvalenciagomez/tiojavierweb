import React, {Component}from 'react';

import '../styles/banner-home.css'
import banner3 from '../assets/images/banners/banner_rails.jpg';
import banner2 from '../assets/images/banners/recycling.jpg';
import banner1 from '../assets/images/banners/20170329_124403.jpg';

let bannerStatus = 1;
const bannerTimer = 7000;

const imgban3 = document.getElementById('imgban3');
const imgban2 = document.getElementById('imgban2');
const imgban1 = document.getElementById('imgban1');


class BannerHome extends Component {    
    constructor() {
        super();
        this.state = {
            screenWidth: 0
        }
    }
    componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions(() => {

                this.bannerLoop();

                let startBannerLoop = setInterval(() => {
                    this.bannerLoop();
                }, bannerTimer);
            })
        );

    }

    updateWindowDimensions(callback) {
        this.setState({
            screenWidth: window.innerWidth
        });
        callback();
    }

    bannerLoop() {
        
        const moveLeft = this.state.screenWidth+'px';
        const moveRight = '-'+this.state.screenWidth+'px';

        if(bannerStatus === 1) {
            document.getElementById('imgban1').style.opacity = '1';
            
            setTimeout(function() {
                
                document.getElementById('imgban1').style.right = '0px';
                document.getElementById('imgban2').style.right = moveRight;
                document.getElementById('imgban3').style.right = moveLeft;
                
            }, 500);
            setTimeout(function() {
                document.getElementById('imgban2').style.opacity = '0';
                document.getElementById('imgban3').style.opacity = '0';                
            }, 1000);
            bannerStatus = 2;
        }
        else if(bannerStatus === 2) {
            document.getElementById('imgban2').style.opacity = '1';
            
            setTimeout(function() {

                document.getElementById('imgban2').style.right = '0px';
                document.getElementById('imgban3').style.right = moveRight;
                document.getElementById('imgban1').style.right = moveLeft;
                
            }, 500);
            setTimeout(function() {
                document.getElementById('imgban3').style.opacity = '0';
                document.getElementById('imgban1').style.opacity = '0';                                
            }, 1000);
            bannerStatus = 3;
        }
        else if(bannerStatus === 3) {
            document.getElementById('imgban3').style.opacity = '1';
            
            setTimeout(function() {

                document.getElementById('imgban3').style.right = '0px';
                document.getElementById('imgban1').style.right = moveRight;
                document.getElementById('imgban2').style.right = moveLeft;
                
            }, 500);
            setTimeout(function() {
                document.getElementById('imgban1').style.opacity = '0';
                document.getElementById('imgban2').style.opacity = '0';
            }, 1000);
            bannerStatus = 1;
        }


    }

    render() {
        return (
                <div className="home-banner" id="home-banner">
                        <img src={banner3} className="imgImgBan" id="imgban3"/>
                        <img src={banner2} className="imgImgBan" id="imgban2" style={{top:'-55px'}} />
                        <img src={banner1} className="imgImgBan" id="imgban1" style={{top:'-105px'}} />
                   
                </div>
        );
    }
   
}


export default BannerHome;