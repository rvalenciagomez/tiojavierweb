import React, {Component} from 'react';

import ArticulosList from './blog/articulos-list';
import BannerHome from './banner-home';
import '../styles/home.css'
// import banner from '../assets/images/grandes/NG_tampa_01.jpg';
import banner from '../assets/images/banners/banner_rails.jpg';
import banner2 from '../assets/images/banners/recycling.jpg';
import img_1_grupo_personas from '../assets/images/1_grupo_personas.jpg';
import img_2_materiales from '../assets/images/2_manejo_materiales.jpg';
import img_3_transportadora_bandas from '../assets/images/3_transportadora_bandas.jpg';



class Home extends Component {

    componentDidMount() {
        window.scrollTo(0,0);
    }
    
    renderBoxes(image1, descripcion1, image2, descripcion2, backgndColor, tituloDeBox) {
        var descFunc1 = () => {
            return (
                <p>SIMMAT constituida por profesionales con más de 20 años de experiencia en el desarrollo de proyectos de manejo de gráneles sólidos, ingeniería especializada en proyectos llave en mano,  en el diseño y fabricación de equipos para la recepción, transporte y almacenamiento de gráneles y otros materiales de difícil manipulación.
                    <br/> Nuestro personal es capacitado continuamente para ofrecer el mejor servicio de asesoramiento para dar la mejor solución a sus Requerimientos y así obtener el mejor costo – beneficio del mercado.                    
                </p>);
        }

        var descFunc2 = () => {
            return (
                <p>Manejo de gráneles eficiente.  Hoy en día la eficiencia y optimización del manejo de gráneles es vital, para mantener los costos de producción,  distribución y almacenamiento competitivos.
                    <br/>Hemos establecido alianzas estratégicas con  importantes empresas de clase mundial, para ofrecer diferentes alternativas y soluciones a nuestros clientes.
                </p>);
        }

        var descFunc3 = () => {
           return (
                <p> 
                    <div style={{marginTop:'-50px'}}>
                        <br/>SIMMAT está localizada estratégicamente en la ciudad de México para cubrir el territorio Mexicano y Centro América.  También contamos con oficinas en Charlotte Carolina del Norte, Estados Unidos para proyectos Internacionales.  Fabricamos en México y Estados Unidos dependiendo  del proyecto.
                        <br/><br/>
                        <strong>SIMMAT se divide en 4 áreas principales:</strong>
                    
                        <br/> <br/>
                    
                        <div style={{ textAlign:'left'}}>
                            1- Desarrollo de proyectos de manejo de materiales a granel sólido (Diseño, Ingeniería, y Fabricación)<br/>
                            2- Representaciones de componentes y equipos para el manejo de solidos (Transportación, rodillos, bandas, cadenas, cangilones etc.)<br/>
                            3- Instalaciones y commissioning (Puesta en marcha ) de cualquier proyecto en manejo de gráneles sólidos.<br/>
                            4- Representaciones exclusivas en para México & Centro América. <br/>
                        </div>
                    </div>
                </p>
            );
        }

        const img1 = image1 
                    ? <img src={image1} className="card_img_custom" height="255" width="391"/>
                    : '';
        const img2 = image2
                    ? <img src={image2} className="card_img_custom" height="255" width="391"/>
                    : '';

        const desc1 = descripcion1 === 'desc1' 
                        ? descFunc1() 
                        : descripcion1 === 'desc3' ?  descFunc3() : '';
        const desc2 = descripcion2 === 'desc2' ? descFunc2() : '';

        // si backgrnd color naranja, entonces letras blancas
        const colorLetra = backgndColor !== 'white' ? 'white' : '';
        return (
            <div className="content-wapper--inner-thinner-sides">
                    <div className="row col-sm-12" style={{padding: 0, backgroundColor:backgndColor, color:colorLetra, margin:0}}>
                        <div className="col-sm-12" >
                            <h3 className="titulo-box centered" >{ tituloDeBox }</h3>
                        </div>

                        <div className="col-sm-6">
                    {/*Planifica tus actividades*/}
                            
                            <div className="card_item">
                            <div className="card__inner">
                                <div className="card_title">
                                <h3>{/*Planifica tus actividades*/}</h3>
                                </div>
                                <div className="card__image">
                                    {img1}
                                </div>

                                {desc1}
                            </div>
                            </div>

                        </div>

                        <div className="col-sm-6">
                        {/*Titulo*/}

                            <div className="card_item">
                                <div className="card__inner">
                                    <div className="card_title">
                                    {/*<h3>TITULO</h3>*/}
                                    </div>
                                    <div className="card__image">
                                        {img2}
                                    </div>

                                    {desc2}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        );
    }


    render() {    
        const tituloBox1 = 'Manejo de Materiales Eficiente!';
        const tituloBox2 = 'Queremos contar con tu confianza, descubre lo que podemos ofrecerte';
        return (
            <div>
                {/*Seccion de inicio*/}
                <div className="row col-xs-12 content-wrapper">
                    <div id="contenedor_slider">
                        <div className="row col-xs-12" >
                            {/* - - Banner - -  */}
                            <BannerHome />
                            {/* <img src={banner2} style={{width:'102%', marginTop:'-129px'}}/> */}

                            <div className="texto">
                                <p className="tit">SIMMAT</p>
                                <p style={{fontSize:'1.4rem', fontWeight: '600'}}>
                                    Materials Handling Systems and Components Experts, S.A.S 
                                </p>
                            </div>
                        </div>
                    </div>
                    {/*<h1 className="titulo">SIMMAT</h1> 
                    <h3 className="subtitulo" >Materials Handling Systems and Components Experts S.A.S <h3>*/}
                    {/*(click)="onGoToCRM()"*/}
                    {/*<button type="button" 
                            className="btn btn-primary button-comenzar" > Contactanos!..</button>*/}
                </div>

                 {/*Seccion 2*/}
                

                {/*------ grid of boxes -----------------*/}
                {/*image1, descripcion1, image2, descripcion2, backgndColor, tituloDeBox*/}
                { this.renderBoxes(null, 'desc1', img_1_grupo_personas, null, 'white', '') }

                { this.renderBoxes(img_2_materiales, null, null, 'desc2', '#FF4F00', tituloBox1) }

                {/* ------ end //  Articulos  ---------- */}

                { this.renderBoxes(null, 'desc3', img_3_transportadora_bandas, null, 'white', tituloBox2) }
                
                {/* ------  Articulos  ---------- */}

                <div className="row col-xs-12" style={{textAlign:'center', padding: '0 2em', margin: '1rem 0 2rem 0', background:'#FF4F00'}}>
                    <div className="col-xs-12" >
                        <h2 style={{ textAlign:'center', marginTop:'20px'}}>Destacados</h2>
                    </div>
                    <ArticulosList { ...this.props }
                        tienePrioridad={true} />
                </div>
                
            </div>
        );
    }
}

export default Home;