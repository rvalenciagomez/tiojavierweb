export const EditFileName = (file_name) => {
    // remplazar espacios
		while (file_name.search(" ") != -1) {
			file_name = file_name.replace(" ","_");
		}
		console.log('file name sin espacios',file_name );

		// remplazar acentos
		var dict = {"á":"a", "é":"e","í":"i","ó":"o","ú":"u", 
					"Á":"A", "É":"E","Í":"I","Ó":"O","Ú":"U"};

		file_name = file_name.replace(/[^\w ]/g, function(char) {
			return dict[char] || char;
		});
		console.log('file name sin acentos',file_name );
        return file_name;
}

