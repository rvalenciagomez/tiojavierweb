/*
    *Email service
*/
//let nodemailer = require('nodemailer');
let moment = require('moment');


// let send = function (text,callback) {
 
//   sails.log.info("Should send text: "+text)
  
//   callback();
// };

export default function sendEmail(to,subject, text, html) {

    let helper = require('sendgrid').mail;

    let mail = new helper.Mail();
    let email = new helper.Email("robertovalenciagomez@gmail.com", "Mas Ventas CRM");
    mail.setFrom(email);

    let personalization = new helper.Personalization();

    // arreglo de correos
    let toArray = to.split(',');
    for (let i = 0; i < toArray.length; i++) {
        email = new helper.Email(toArray[i]);
        personalization.addTo(email);
    }
    
    // subject
    personalization.setSubject(subject);
    
    //header
    let header = new helper.Header("X-Test", "True");
    // let header = new helper.Header("Access-Control-Allow-Origin", "*");
    personalization.addHeader(header);
    
    mail.addPersonalization(personalization);

    // content text o html
    let content = new helper.Content("text/plain", text);
    mail.addContent(content);
    content = new helper.Content("text/html", html);
    mail.addContent(content);


    // config de enviar mail
    //process.env.SENDGRID_API_KEY
        // let sg = require('sendgrid')('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');
        // let sg = require('sendgrid')(process.env.SENDGRID_API_KEY);    
        let sg = require('sendgrid')('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');    
        let request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
        });

    // enviar mail
    let requestBody = mail.toJSON();
    let emptyRequest = require('sendgrid-rest').request;
    let requestPost = JSON.parse(JSON.stringify(emptyRequest));
    // requestPost.headers = {
    //     'Content-Type': 'application/json' , 
    //     'Access-Control-Allow-Origin': 'http://sendgrid.com',
    //     'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
    //     'Access-Control-Allow-Headers':'X-Requested-With'   
    // };
    requestPost.method = 'POST';
    requestPost.path = '/v3/mail/send';
    requestPost.body = requestBody;
    sg.API(requestPost, function (error, response) {
        console.log('response.statusCode ',response.statusCode);
        console.log('response.body',response.body);
        console.log('response.headers',response.headers);
        (error) => {
            console.log('error en email: ',error);
        }
    })
};