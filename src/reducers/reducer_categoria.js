import _ from 'lodash';
import { CREATE_CATEGORIA, FETCH_CATEGORIAS, DELETE_CATEGORIA, EDIT_CATEGORIA } from '../actions/type';

export default function(state ={}, action) {
    switch (action.type) {
        case EDIT_CATEGORIA:
            return { ...state, [action.payload.key]: action.payload };

        case CREATE_CATEGORIA:
            return { ...state, [action.payload.key]: action.payload };

        case FETCH_CATEGORIAS:
            if(action.payload) {
                let newObj =  _.mapKeys(action.payload, (value,key) => key);
                // console.log('categorias in reducer', newObj);
                return newObj;
            } else {
                return state;
            }

        case DELETE_CATEGORIA:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}