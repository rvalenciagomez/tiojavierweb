import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import ArticulosReducers from './reducer_articulo';
import ContactoReducer from './reducer_contacto';
import CategoriaReducer  from './reducer_categoria';
import AuthReducer from './auth_reducer';


const rootReducer = combineReducers({
    articulos: ArticulosReducers,
    form: formReducer,
    contactos: ContactoReducer,
    auth: AuthReducer,
    categorias: CategoriaReducer
});

export default rootReducer;
