import _ from 'lodash';
import { EDIT_CONTACTO } from '../actions';

export default function(state ={}, action) {
    switch (action.type) {
        case EDIT_CONTACTO:
            return { ...state, contacto: action.payload };
        default:
            return state;
    }
}