import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, FETCH_MESSAGE, AUTH_PROFILE } from '../actions/type';

export default function (state={ authenticated: false }, action) {
    switch (action.type) {
        case AUTH_USER:
            return { ...state, authenticated: true };
        case UNAUTH_USER:
            return { ...state, authenticated: false };
        case AUTH_ERROR:
            return { ...state, error: action.payload };
        case FETCH_MESSAGE:
            return { ...state, message: action.payload };
        case AUTH_PROFILE:
            return { ...state, profile: action.payload }
        default:
            return state;
    }
}