import _ from 'lodash';
import { FETCH_ARTICULOS, DETAIL_ARTICULOS, EDIT_ARTICULOS, DELETE_ARTICULOS }  from '../actions';

export default function(state = {}, action) {
    switch (action.type) {
        case EDIT_ARTICULOS:
            let newObjEdit = {...state, [action.payload.key]: action.payload }
            // console.log('reducer de edit articulos', newObjEdit);        
            return newObjEdit;

        case DETAIL_ARTICULOS:
            let newObj = {...state, [action.payload.key]: action.payload }
            // console.log('reducer de detail articulos', newObj);        
            return newObj;

        case FETCH_ARTICULOS: 
            let newState = _.mapKeys(action.payload, (value,key) => key);
            newState = _.omit(newState, 'categoria');
            // console.log('pasa por fetch articulos', newState);
            return newState;
        case DELETE_ARTICULOS:
            return _.omit(state, action.payload)
        default:
            return state;
    }
}