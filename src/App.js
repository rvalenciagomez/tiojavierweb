import reducers from './reducers/index'
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import React, { Component } from 'react';
import './App.css';
import Home from './components/home'
import Menu from './components/menu'
import Footer from './components/footer'
import { Provider } from 'react-redux';
import Blog from './components/blog/blog';
import AddArticulo from './components/blog/add-articulo';
import EditArticulo from './components/blog/edit-articulo';
import EditCategoriasArticulos from './components/blog/edit-categorias-articulos';
import DetailArticulo from './components/blog/detail-articulo';
import AddContacto from './components/contacto/add-contacto';
import QuienesSomos from './components/quienes-somos';
import Signin from './components/auth/signin';
import Signup from './components/auth/signup';
import Logout from './components/auth/logout';
import requireAuth from './components/auth/require_authentication.js';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(reducers);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter> 
          <div>
              <Menu />
            <Switch>  
              <Route exact path="/add-contacto" component={AddContacto} />
              <Route exact path="/catalogo/add-articulo" component={ requireAuth(AddArticulo) } />
              <Route exact path="/catalogo/edit-articulo/:id" component={ requireAuth(EditArticulo) } />
              <Route exact path="/catalogo/edit-categorias" component={ requireAuth(EditCategoriasArticulos) } />              
              <Route path="/catalogo/:id" component={DetailArticulo} />              
              <Route exact path="/catalogo" component={Blog} />
              <Route exact path="/quienes-somos" component={QuienesSomos} />
              <Route exact path="/signin" component={Signin} />
              <Route exact path="/signup" component={Signup} />
              <Route exact path="/logout" component={Logout} />              
              <Route path="/" component={Home} />
            </Switch>
              <Footer />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
