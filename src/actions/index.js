import _ from 'lodash';
import { reset } from 'redux-form';
import firebase from 'firebase';
import React from 'react';
import { EditFileName } from '../services/blog.service'
import sendEmail from '../services/email.service';
import Articulo from '../models/articulo';
import axios from 'axios';
import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, FETCH_MESSAGE, AUTH_PROFILE,
         CREATE_CATEGORIA, FETCH_CATEGORIAS, DELETE_CATEGORIA, EDIT_CATEGORIA } from './type';
import { signInFb } from '../services/firebase.service';
import reactfire from 'reactfire';
import { roleOfUser } from '../models/role-user';
import { AMAZON_BUCKET } from './constantes';

// amazon files connection
let AWS = require('aws-sdk');

// firebase config
var config = {
    apiKey: "AIzaSyDWo0b8iA6erxiGrS9ZR7T93IePGL9teGc",
    authDomain: "simmat-web.firebaseapp.com",
    databaseURL: "https://simmat-web.firebaseio.com",
    projectId: "simmat-web",
    storageBucket: "simmat-web.appspot.com",
    messagingSenderId: "1083580541152"
}

// firebase connect
firebase.initializeApp(config);

//firebase tables
let database = firebase.database();

const Articulos =  database.ref('articulo/');
const Contactos =  database.ref('contacto/');
const Categorias =  database.ref('articulo/categoria/');
const Users =  database.ref('user/');

const SERVER_LOCALHOST_URL = 'http://localhost:1337';
const SERVER_HEROKU_URL = 'https://crm-backend-v3.herokuapp.com';

export const EDIT_CONTACTO = 'edit contacto';
export const FETCH_ARTICULOS = 'FETCH_ARTICULOS';
export const CREATE_ARTICULOS = 'CREATE_ARTICULOS';
export const DETAIL_ARTICULOS = 'DETAIL_ARTICULO' ;
export const EDIT_ARTICULOS = 'EDIT_ARTICULOS';
export const DELETE_ARTICULOS = 'DELETE_ARTICULOS';

let AWSService = AWS;
// var s3 = new AWS.S3();

const amazonRoute = "https://s3.amazonaws.com/";
const bucketAmazon = AMAZON_BUCKET;
AWSService.config.accessKeyId = 'AKIAIWS4NJ25WZC77XDQ';
AWSService.config.secretAccessKey = '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';

export function editCategoria(values) {
    Categorias.child(values.key).update(values).then();

    return dispatch => {
        dispatch({
            type: EDIT_CATEGORIA,
            payload: values
        });
    }
}

export function deleteCategoria(key) {
    return dispatch => {
        //revisar si hay una categoria en algun articulo
            // si la hay no eliminar
        Articulos.on('value', snapshot =>  {
                let articulosObj = snapshot.val();

                let articulosArr =[];
                
                articulosArr = Object.keys(articulosObj).map(key => articulosObj[key]);
                console.log('articulos con categoria', articulosArr);

                articulosArr = articulosArr.filter(art => art.categoria && art.categoria.value && art.categoria.value === key );

                if(articulosArr.length === 0) {
                    console.log('se eliminará la categoria', articulosArr);
                    console.log('key categ to delete',key)
                    
                    Categorias.child(key).remove().then(  

                        dispatch({
                            type: DELETE_CATEGORIA,
                            payload: key
                        })
                    );
        
                } else {
                    console.log('ya existe, y no puede ser eliminada la categoria', articulosArr);
                }
        });
    } // dispatch end
}

// traer categorias
export function fetchCategorias() {
    return dispatch => {
        Categorias.on('value', snapshot =>  {
            let categoriasObj = snapshot.val();

            //hacer arreglo de keys
            let categoriasArr = [];
            if(categoriasObj) {
                categoriasArr = Object.keys(categoriasObj).map(key => {
                    let post = categoriasObj[key];
                    post.key = key;
                    return post;
                });

            //cambiar a un objeto de categorias
            categoriasObj = _.mapKeys(categoriasArr,'key');
            } else {
                categoriasObj = { 'fake':{ key:'fake', nombre:'Categoria de Prueba' } };   
            }
            
            dispatch({
                type: FETCH_CATEGORIAS,
                payload: categoriasObj
            });
        });
    }
}

// crear categoria nueva
export function crearCategoria(categoria) {
    // agregar categoria a firebase
    return dispatch => {                        
        Categorias.push(categoria)
            .then(({key}) => {
                // agregar key a categoria                        
                categoria.key = key;
                    // agregar Articulo a state
                    dispatch({
                        type: CREATE_CATEGORIA,
                        payload: categoria
                    });
            });
    }
}

// revisar si usuario está logeado
export function onUserAlreadyLogged() {
    return dispatch => {
        firebase.auth().onAuthStateChanged((user) => {
            if(user) {
                // user is signed in 
                console.log('user from firebase already logged ',user);
                
                dispatch({ type: AUTH_USER });
                
                guardarProfileEnLocalStorage(user.email, user.uid, (profile)=> {
                    dispatch({
                        type:AUTH_PROFILE,
                        payload: profile
                    });
                });
            } else {
                // no user signed in
                console.log('user from firebase still not logged ');
            }
        });
    }
}


export function signupUser({nombre, telefono , email, password}, callback) {
    const userNew = { nombre, telefono, email, password };
    // set role to USER*
    userNew.role =  roleOfUser.USER;

    return dispatch => {

        // en firebase authentication
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(res => {
                console.log('respuesta de auth',res);
                console.log('res uid',res.uid);
                // authenticar true
                dispatch({ type: AUTH_USER });
                // regresar callback
                callback();
                // salvar usuario
                saveUserDb(userNew, res.uid, (profile)=> {
                    dispatch({
                        type:AUTH_PROFILE,
                        payload: profile
                    });
                });
            })
            .catch((error) => {
                console.log('error de auth',error);
                dispatch({
                    type: AUTH_ERROR,
                    payload: error.message
                });
            });
    }
}

function saveUserDb(userNew, uid, callback) {

    Users.on('value', snapshot =>  {
        let usersObj = snapshot.val();
        console.log('users', usersObj);
        // Crear arreglo de keys
        let usersArr = [];

        usersArr = Object.keys(usersObj).map(key => {
            let post = usersObj[key];
            post.key = key;
            return post;
        });
        console.log('usersArr', usersArr);
        // filtrar correos iguales
        const userList =  usersArr.filter(user =>  user.email === userNew.email);
        // if userNew email, ya existe en usersArr no agregar
        if(userList.length === 0) {
            // agregar userNew a base de datos
            database.ref('user/' + uid)
                .set(userNew);
        }

        // salvar profile en localstorage
        let userLogged = {
            nombre: userNew.nombre,
            telefono: userNew.telefono,
            email: userNew.email,
            key: uid,
            role: userNew.role
        };

        localStorage.setItem('profile', JSON.stringify(userLogged) );
        const profile = JSON.parse( localStorage.getItem('profile') );
        console.log('user profile', profile );

        callback(profile);
    });
}

export function signinUser({email, password}, callback) {
    return dispatch => {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(res => {
                console.log('respuesta de auth',res);

                // salvar en local storage
                guardarProfileEnLocalStorage(email, res.uid, (profile)=> {
                    dispatch({
                        type:AUTH_PROFILE,
                        payload: profile
                    });
                });
                
                dispatch({ type: AUTH_USER });

                callback();
            })
            .catch((error) => {
                console.log('error de auth',error);
                dispatch({
                    type: AUTH_ERROR,
                    payload: error.message
                });
            });
    }
}

// salvar en local storage
function guardarProfileEnLocalStorage(email, uid, callback) {
    // traer user guardado
    Users.on('value', snapshot =>  {
        const { nombre, telefono, role } = snapshot.val()[uid];
        // console.log('user from fb', users[uid]);
        
        // crear objeto user profile
        let userLogged = {
            nombre,
            telefono,
            email,
            key: uid,
            role
        };
        localStorage.setItem('profile', JSON.stringify(userLogged) );
        const profile = JSON.parse( localStorage.getItem('profile') );
        console.log('user profile', profile );

        callback(profile);
    });
    
}

export function logoutUser() {
    return dispatch => {
        firebase.auth().signOut()
            .then(
                () => { 
                    console.log('Logged auth successfully'); 
                    
                    dispatch({ type: UNAUTH_USER });

                    dispatch({
                        type:AUTH_PROFILE,
                        payload: null
                    });

                    localStorage.removeItem('profile');
                    console.log('user profile after logout', localStorage.getItem('profile') );
                }, 
                (error)=> { console.log('Unable to logout'); }
            );


    }
}

export function crearContacto(contacto, callback) {  //, to,subject, text, html
    return dispatch => {

    Contactos.on('value', snapshot =>  {
            let contactosObj = snapshot.val();
            console.log('contactos', contactosObj);
            //hacer arreglo de keys
            let contactosArr = [];
            contactosArr = Object.keys(contactosObj).map(key => {
                let post = contactosObj[key];
                post.key = key;
                return post;
            });
            console.log('contactosArr', contactosArr);
        // filtrar correos iguales
        const contacList =  contactosArr.filter(contac =>  contac.email === contacto.email);
        // if contacto email, ya existe en contactosArr no agregar
        if(contacList.length === 0) {
            // agregar contacto a base de datos
            Contactos.push(contacto);
        }
    });

    //enviar correo a administrador
     const { to, subject, html, text } = sendEmailDeContacto(contacto);

     console.log('parametro para email',to, subject, html, text );

     axios.post(`${SERVER_HEROKU_URL}/sendemail`, { to, subject, html, text })
            .then(()=> callback());

    dispatch(reset('AddContactoForm'));

    } // fin de dispatch
}

export function detailArticulo(key) {
    const articulo = Articulos.child(key);

    return dispatch => {
        articulo.on('value', snapshot => {
            let post = snapshot.val();
            // articulo ? post.key = articulo.key() : post = null ;

            dispatch({
                type: DETAIL_ARTICULOS ,
                payload: post
            })
        });
    };
}

export function editArticulo(values, callback) {
    Articulos.child(values.key).update(values).then(()=> callback());

    return dispatch => {
        dispatch({
            type: EDIT_ARTICULOS,
            payload: values
        });
    }
}

export function editContent(values, content) {
    values.descripcion_detallada = '';
    console.log('articulo con content en actions', values);
}

export function createArticulo(values, callback) {
    return dispatch => {
        Articulos.push(values).then(()=> callback());
    }

}

export function fetchArticulos() {
    return dispatch => {
        Articulos.on('value', snapshot =>  {
            let articulosObj = snapshot.val();

            //hacer arreglo de keys
            let articulosArr = [];

            if(articulosObj) {
                articulosArr = Object.keys(articulosObj).map(key => {
                    let post = articulosObj[key];
                    post.key = key;
                    return post;
                });

                //cambiar a un objeto de articulos
                articulosObj = _.mapKeys(articulosArr,'key');
            }
            dispatch({
                type: FETCH_ARTICULOS,
                payload: articulosObj
            });
        })
    }
}


export function deleteArticulo(key, callback) {
    Articulos.child(key).remove().then(() => callback());
    
    return dispatch => {
        dispatch({
            type: DELETE_ARTICULOS,
            payload: key
        })

    }
}


export function uploadFiles(indexFile, filesArray, articulo) {

    let file_name = uploadToAmazon(filesArray);

    // si no existe el arreglo se crea
    if(!articulo.filesName) {
        articulo.filesName = [];
    }
    // se agrega el nombre del archivo
    console.log('indexFile', indexFile);
    debugger;

    if(indexFile === null || indexFile === -1) {
        articulo.filesName.push(file_name);
    } else {
        articulo.filesName[indexFile] = file_name;
    }
    console.log('articulo files', articulo);

    //articulo.filesName.push({ id: articulo.filesName.length + 1, name:file_name });

    //update en firebase
    Articulos.child(articulo.key).update(articulo);

    return dispatch => {
        console.log('articulo en action edit', articulo);
        dispatch({
            type: EDIT_ARTICULOS,
            payload: articulo
        });
    }

}

export function deleteImage( file_name, articulo ) {
    // si no existe el arreglo se crea
    if(!articulo.filesName) {
        articulo.filesName = [];
    }
    // sacar index de file
    let indexFile = _.findIndex(articulo.filesName, file => file == file_name );
    // si existe index eliminar elemento
    if(indexFile !== null && indexFile !== -1) {
        articulo.filesName.splice(indexFile,1);
    }
    console.log(articulo.filesName)
    //update en firebase
    Articulos.child(articulo.key).update(articulo);
    
    return dispatch => {
        dispatch({
            type: EDIT_ARTICULOS,
            payload: articulo
        });
    }
}

var uploadToAmazon = function(filesArray) {
    let file = filesArray[0];
    
    let file_name = file.name;
    console.log('file name antes',file_name );

    //edit file name
    file_name = EditFileName(file_name);
    console.log('file name despues',file_name );

    console.log(AWSService);

    var bucket = new AWSService.S3({params: {Bucket: bucketAmazon}});
    var params = {Key: file_name, Body: file};
    bucket.upload(params, function (err, res) {
        console.log('error', err);
        console.log('response',res);
    });

    return file_name;
}

var sendEmailDeContacto = function(contacto) {

    const to = 'contacto@simmat.mx,robertovalenciagomez@gmail.com';
    const subject = `Nueva solicitud de contacto: ${contacto.nombre}`;
    const text = 'Nuevo contacto de la pagina SIMMAT';
    const html = `<h3>NUEVO CONTACTO:</h3>
        <strong>Nuevo contacto de la pagina SIMMAT</strong><br/>

        <p> <strong>Nombre:</strong> <span> ${contacto.nombre} </span> </p> 
        <p> <strong>Teléfono:</strong> <span> ${contacto.telefono} </span> </p> 
        <p> <strong>Email:</strong> <span> ${contacto.email} </span> </p> 
        <p> <strong>Comentario:</strong> <span> ${contacto.comentario} </span> </p> 
        <p> <strong>Empresa:</strong> <span> ${contacto.empresa} </span> </p> `;

    return {
        to: to,
        subject: subject,
        text: text,
        html: html
    };
   //sendEmail(to,subject, text, html);
}