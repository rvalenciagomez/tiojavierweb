
export default class Articulo {
    id;
    titulo;
    descripcion;
    descripcion_detallada;
    filesName = { id:null, name:'' };
    publicado;
    destacado;
    fecha_publicacion;
    createdAt;
    updatedAt;
}